package org.game.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashMap;
import java.util.List;

@Data
@AllArgsConstructor
public class PlayerInsight {
  //player x played with y and won
  // player x won with tougher/easier opponent based on ranking
  // etc
  HashMap<String , List<Match>> playerInsight;
}
