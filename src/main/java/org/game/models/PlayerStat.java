package org.game.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PlayerStat {
  int score;
  int ranking;
  int won;
  int lost;
}
