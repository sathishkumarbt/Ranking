package org.game.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Player {
  int id;
  String name;
  int Rank;
  int win;
  int loss;
}
