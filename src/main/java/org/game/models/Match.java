package org.game.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Match {
  int playerA;
  int playerB;
  int winnerId;
}
