package org.game.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class GameStatsReport {
  List<PlayerStat> playersStats;
  List<PlayerInsight> playerInsight;
  List<Fixture> nextMatches;
}
