package org.game.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Fixture {
  Player playerA;
  Player playerB;
}
